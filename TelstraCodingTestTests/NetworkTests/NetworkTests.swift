//
//  NetworkTests.swift
//  TelstraCodingTestTests
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import XCTest
@testable import TelstraCodingTest

class NetworkTests: XCTestCase {

    func testInvalidUrl() {
        let resultExpectation = expectation(description: "result expectation")
        let network = Network<String>("Invalid End Point")
        var result: Result<String, NetworkError>?
        
        network.getItem("") { r in
            result = r
            resultExpectation.fulfill()
        }
        
        waitForExpectations(timeout: 1, handler: nil)
        
        XCTAssertNotNil(result)
        XCTAssert(result! == Result<String, NetworkError>.failure(.badURL))
        
    }

}
