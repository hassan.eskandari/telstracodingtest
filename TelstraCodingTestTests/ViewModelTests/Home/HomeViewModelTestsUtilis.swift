//
//  TestUtils.swift
//  TelstraCodingTestTests
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import Foundation
@testable import TelstraCodingTest

final class TestHomeNavigator: HomeNavigator {
    func toHome() { }
}

final class TestInformationApiProvider: InformationApiProvider {
    
    static let jsonString = """
{
  "title": "About Canada",
  "rows": [
    {
      "title": "Beavers",
      "description": "Beavers are second only to humans in their ability to manipulate and change their environment. They can measure up to 1.3 metres long. A group of beavers is called a colony",
      "imageHref": "http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/American_Beaver.jpg/220px-American_Beaver.jpg"
    },
    {
      "title": "Flag",
      "description": null,
      "imageHref": "http://images.findicons.com/files/icons/662/world_flag/128/flag_of_canada.png"
    }
  ]
}
"""
   
    static var information: Information {
        let data = jsonString.data(using: .utf8)!
        return try! JSONDecoder().decode(Information.self, from: data)
    }
    
    var delay: Double = 0.0
    var shouldSucceed = true
    func information(_ callback: @escaping Callback) {
        func performTask() {
            if self.shouldSucceed {
                callback(.success(TestInformationApiProvider.information))
            } else {
                callback(.failure(.badURL))
            }
        }
        if delay > 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
                performTask()
            }
        } else {
            performTask()
        }
        
    }
}
