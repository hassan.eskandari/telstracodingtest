//
//  HomeViewModelTests.swift
//  TelstraCodingTestTests
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import XCTest
@testable import TelstraCodingTest

class HomeViewModelTests: XCTestCase {

    var viewModel: HomeViewModel!
    var observation: NSKeyValueObservation!
    var observationValues: [Bool]!
    var apiProvider: InformationApiProvider!
    
    override func setUp() {
        observationValues = [Bool]()
        apiProvider = TestInformationApiProvider()
        viewModel = HomeViewModel(api: apiProvider, navigator: TestHomeNavigator())
        observation = viewModel.output.observe(\.loadingData) { (output, change) in
            self.observationValues.append(output.loadingData)
        }
    }
    
    
    func testFetchingInfo() {
        viewModel.reloadData()
        
        XCTAssertEqual([true, false], observationValues)
        XCTAssertFalse(viewModel.output.loadingData)
        XCTAssertEqual(TestInformationApiProvider.information.title, viewModel.title)
        XCTAssertEqual(TestInformationApiProvider.information.rows, viewModel.records)
    }
    
    func testFetchingInfoFailure() {
        (apiProvider as! TestInformationApiProvider).shouldSucceed = false
        viewModel.reloadData()
        
        XCTAssertEqual([true, false], observationValues)
        XCTAssertFalse(viewModel.output.loadingData)
        XCTAssertEqual("Home", viewModel.title)
        XCTAssertEqual([], viewModel.records)
    }
    
    func testIgnoreRepetitiveReload() {
        let waiter = expectation(description: "wait")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            waiter.fulfill()
        }
        
        (apiProvider as! TestInformationApiProvider).delay = 2
        viewModel.reloadData()
        (apiProvider as! TestInformationApiProvider).delay = 0
        viewModel.reloadData()
        
        waitForExpectations(timeout: 3, handler: nil)
        
        XCTAssertEqual([true, false], observationValues)
    }

}
