//
//  InformationTests.swift
//  TelstraCodingTestTests
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import XCTest
@testable import TelstraCodingTest

class InformationTests: XCTestCase {

    func testInit() {
        let information = Information(title: "title", rows: [InformationRecord]())
        
        XCTAssertEqual("title", information.title)
        XCTAssertEqual([], information.rows)
    }
    
    func testEquatable() {
        let information = Information(title: "title", rows: [InformationRecord]())
        let anotherInformation = Information(title: "anotherTitle", rows: [InformationRecord]())

        XCTAssertTrue(information == information)
        XCTAssertFalse(information == anotherInformation)
    }

}
