//
//  AppDelegate.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/2/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let window = UIWindow(frame: UIScreen.main.bounds)

        Application.shared.configureMainInterface(in: window)

        window.makeKeyAndVisible()
        self.window = window

        return true
    }
}

