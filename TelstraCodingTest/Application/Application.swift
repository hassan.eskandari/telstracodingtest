//
//  Application.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import Foundation
import UIKit


final class Application {
    static let shared = Application()
    
    public func configureMainInterface(in window: UIWindow) {
        
        let networkProvider = NetworkProvider()
        
        let navigattionController = UINavigationController()
        let navigator = DefaultHomeNavigator(services: networkProvider,
                                             navigationController: navigattionController)
        
        window.rootViewController = navigattionController
        
        navigator.toHome()
        
    }
}
