//
//  NetworkProvider.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import Foundation

final class NetworkProvider {
    private let apiEndpoint: String

    public init() {
        apiEndpoint = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    }

    public func makeInformationApi() -> InformationApiProvider {
        let network = Network<Information>(apiEndpoint)
        return InformationApi(network: network)
    }

}
