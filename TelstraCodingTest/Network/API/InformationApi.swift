//
//  InformationApi.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import Foundation
protocol InformationApiProvider {
    typealias ResultType = Result<Information, NetworkError>
    typealias Callback = (ResultType) -> Void

    func information(_ callback: @escaping Callback)
}

final class InformationApi: InformationApiProvider {
    private let network: Network<Information>
    
    init(network: Network<Information>) {
        self.network = network
    }
    
    func information(_ callback: @escaping Callback) {
        return self.network.getItem("", callback: callback)
    }
}
