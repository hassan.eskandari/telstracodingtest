//
//  NetworkError.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case badURL
    case invalidResponse
    case noData
    case invalidData
    case genericError(error: Error)
}

extension NetworkError: Equatable {
    static func == (lhs: NetworkError, rhs: NetworkError) -> Bool {
        switch (lhs, rhs) {
        case (.badURL, .badURL):
            return true
        case (.invalidResponse, .invalidResponse):
            return true
        case (.noData, .noData):
            return true
        case (.invalidData, .invalidData):
            return true
        case (.genericError(let a), .genericError(let b)):
            return a.localizedDescription == b.localizedDescription
        default:
            return false
        }
    }
}
