//
//  Network.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import Foundation

final class Network<T: Decodable> {
    
    typealias ResultType = Result<T, NetworkError>
    typealias NetworkCallback = (ResultType) -> Void
    
    private let endPoint: String
    
    init(_ endPoint: String) {
        self.endPoint = endPoint
    }
    
    private func getAbsolutePath(_ path: String) -> String {
        if path.starts(with: endPoint) {
            return path
        } else {
            return "\(endPoint)/\(path)"
        }
    }
    
    func getItem(_ path: String, callback: @escaping NetworkCallback) {
        let path = getAbsolutePath(path)
        
        guard let url = URL(string: path) else {
            callback(.failure(.badURL))
            return
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            guard let dataString = try? String(contentsOf: url, encoding: .isoLatin1) else {
                self.failureOnMainThread(.noData, callback)
                return
            }
            
            guard let resultData = dataString.data(using: .utf8) else {
                self.failureOnMainThread(.noData, callback)
                return
            }
            
            guard let responseData = try? JSONDecoder().decode(T.self, from: resultData) else {
                self.failureOnMainThread(.invalidData, callback)
                return
            }
            
            self.sucessOnMainThread(responseData, callback)
        }
    }
    
    private func failureOnMainThread(_ error: NetworkError, _ callback: @escaping NetworkCallback) {
        DispatchQueue.main.async {
            callback(.failure(error))
        }
    }
    
    private func sucessOnMainThread(_ data: T, _ callback: @escaping NetworkCallback) {
        DispatchQueue.main.async {
            callback(.success(data))
        }

    }
    
}
