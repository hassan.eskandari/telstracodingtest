//
//  HomeTableViewCell.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import UIKit
import Kingfisher

class HomeTableViewCell: UITableViewCell {
    
    private var titleLabel: UILabel!
    private var descriptionLabel: UILabel!
    private var cellImageView: UIImageView!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initUI()
    }
    
    func bind(_ model: InformationRecord) {
        titleLabel.text = model.title ?? "No Title"
        descriptionLabel.text = model.description ?? "No Value"
        cellImageView.kf.setImage(with: model.imageUrl, placeholder: UIImage(named: "NoImage"))
    }
    
    private func initUI() {
        
        titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        titleLabel.textAlignment = .left
        titleLabel.lineBreakMode = .byWordWrapping
        titleLabel.font = UIFont.systemFont(ofSize: 16.0, weight: .bold)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        descriptionLabel = UILabel()
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textAlignment = .left
        descriptionLabel.lineBreakMode = .byWordWrapping
        descriptionLabel.font = UIFont.systemFont(ofSize: 14.0, weight: .regular)
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        
        cellImageView = UIImageView()
        cellImageView.contentMode = .scaleAspectFit
        cellImageView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(cellImageView)
        
        NSLayoutConstraint.activate([
            cellImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            cellImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16.0),
            cellImageView.heightAnchor.constraint(equalToConstant: 72.0),
            cellImageView.widthAnchor.constraint(equalToConstant: 72.0)
        ])
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8.0),
            titleLabel.leadingAnchor.constraint(equalTo: cellImageView.trailingAnchor, constant: 8.0),
            titleLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8.0),
            titleLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 20)
        ])
        
        NSLayoutConstraint.activate([
            descriptionLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4.0),
            descriptionLabel.leadingAnchor.constraint(equalTo: cellImageView.trailingAnchor, constant: 8.0),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8.0),
            descriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8.0),
            descriptionLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 30)
        ])
    }
    
}
