//
//  ViewController.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/2/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import UIKit

class HomeViewController: UITableViewController  {
    
    var viewModel: HomeViewModel!
    var observation: NSKeyValueObservation!
    var errorObservation: NSKeyValueObservation!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Telstra Coding Test"
        
        setupTableView()
        setupObservations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refresh()
    }
    
    private func setupTableView() {
        tableView.registerCell(ofType: HomeTableViewCell.self)
        tableView.refreshControl = UIRefreshControl()
        tableView.estimatedRowHeight = 64
        tableView.rowHeight = UITableView.automaticDimension
        tableView.dataSource = self
        
        tableView.refreshControl?.addTarget(self, action: #selector(refresh), for: .allEvents)
    }
    
    private func setupObservations() {
        observation = viewModel.output.observe(\.loadingData) { (output, change) in
            if output.loadingData {
                self.tableView.refreshControl?.beginRefreshing()
            } else {
                self.tableView.refreshControl?.endRefreshing()
                self.title = self.viewModel.title
                self.tableView.reloadData()
            }
        }
        
        errorObservation = viewModel.output.observe(\.errorMessage) { (output, change) in
            if output.errorMessage.isEmpty == false {
                
                print(output.errorMessage)

                let alert = UIAlertController(title: "Error Receiving Data From Server", message: "Check your network connection!", preferredStyle: .alert)
                
                let retryAction = UIAlertAction(title: "Retry", style: .default) { _ in
                    self.refresh()
                }
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { _ in
                }

                alert.addAction(retryAction)
                alert.addAction(cancelAction)
                
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @objc func refresh() {
        viewModel.reloadData()
    }
}

extension HomeViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.records.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ofType: HomeTableViewCell.self, at: indexPath)
        
        cell.bind(viewModel.records[indexPath.row])
        
        return cell
    }
}

