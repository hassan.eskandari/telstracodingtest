//
//  HomeViewModel.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import Foundation

@objc final class HomeViewModelOutput: NSObject {
    @objc dynamic var loadingData: Bool = false
    @objc dynamic var errorMessage: String = ""
}

final class HomeViewModel {
    
    private let api: InformationApiProvider
    private let navigator: HomeNavigator
    
    private(set) var title = "Home"
    private(set) var records = [InformationRecord]()
    
    private(set) var output = HomeViewModelOutput()

    init(api: InformationApiProvider, navigator: HomeNavigator) {
        self.api = api
        self.navigator = navigator
    }
    
    public func reloadData() {
        guard output.loadingData == false else {
            return
        }
        
        output.loadingData = true
        output.errorMessage = ""
        
        api.information { [weak self] result in
            switch result {
            case .failure(let error):
                self?.output.errorMessage = error.localizedDescription
                break
            case .success(let information):
                self?.title = information.title
                self?.records = information.rows.filter { $0.hasValue }
                break
            }
            
            self?.output.loadingData = false
        }
    }
    
}
