//
//  HomeNavigator.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import UIKit

protocol HomeNavigator {
    func toHome()
}

class DefaultHomeNavigator: HomeNavigator {

    private let navigationController: UINavigationController
    private let services: NetworkProvider

    init(services: NetworkProvider,
         navigationController: UINavigationController) {
        self.services = services
        self.navigationController = navigationController
    }

    func toHome() {
        let homeViewController = HomeViewController()
        homeViewController.viewModel = HomeViewModel(api: services.makeInformationApi(), navigator: self)
        navigationController.pushViewController(homeViewController, animated: true)
    }
}
