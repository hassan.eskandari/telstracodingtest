//
//  InformationRecord.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import Foundation

struct InformationRecord: Codable {
    
    public let title: String?
    public let description: String?
    public let image: String?
    
    public var hasValue: Bool {
        return title != nil
            || description != nil
            || image != nil
    }
    
    public var imageUrl: URL? {
        guard let image = image else { return nil }
        return URL(string: image)
    }
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case description = "description"
        case image = "imageHref"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        image = try values.decodeIfPresent(String.self, forKey: .image)
    }

}

extension InformationRecord: Equatable { }
