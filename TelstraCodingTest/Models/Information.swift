//
//  Information.swift
//  TelstraCodingTest
//
//  Created by Hassan on 4/3/20.
//  Copyright © 2020 hassaneskandari. All rights reserved.
//

import Foundation

struct Information: Codable {
    
    public let title: String
    public let rows: [InformationRecord]

    init(title: String, rows: [InformationRecord]) {
        self.title = title
        self.rows = rows
    }
}

extension Information: Equatable {
    static func == (lhs: Information, rhs: Information) -> Bool {
        return lhs.title == rhs.title
            && lhs.rows == rhs.rows
    }
}
